import { ActionReducerMap } from '@ngrx/store';

import { IAppState } from '../state/app.state';
import { ticketReducers } from './ticket.reducers';

export const appReducers: ActionReducerMap<IAppState, any> = {
  tickets: ticketReducers,
};
