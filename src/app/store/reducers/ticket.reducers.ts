import { ETicketActions } from '../actions/ticket.actions';
import { TicketActions } from '../actions/ticket.actions';
import { initialTicketState, ITicketState } from '../state/ticket.state';

export const ticketReducers = (
  state = initialTicketState,
  action: TicketActions
): ITicketState => {
  switch (action.type) {
    case ETicketActions.GetTickets: {
      return state;
    }

    case ETicketActions.CreateTicket: {
      return {
        tickets: state.tickets ? [...state.tickets, action.payload] : [action.payload],
      };
    }

    default:
      return state;
  }
};
