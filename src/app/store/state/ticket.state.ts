import { ITicket } from '../../models/ticket.interface';

export interface ITicketState {
  tickets: ITicket[];
}

export const initialTicketState: ITicketState = {
  tickets: null,
};
