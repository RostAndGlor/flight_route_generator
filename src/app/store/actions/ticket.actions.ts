import { Action } from '@ngrx/store';

import { ITicket } from '../../models/ticket.interface';

export enum ETicketActions {
  GetTickets = '[Ticket] Get Tickets',
  CreateTicket = '[Ticket] Create Ticket'
}

export class GetTickets implements Action {
  public readonly type = ETicketActions.GetTickets;
}

export class CreateTicket implements Action {
  public readonly type = ETicketActions.CreateTicket;
  constructor(public payload: ITicket) {}
}

export type TicketActions = GetTickets | CreateTicket;
