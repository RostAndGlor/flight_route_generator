import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { Store } from '@ngrx/store';

import { IAppState } from 'src/app/store/state/app.state';
import { CreateTicket } from 'src/app/store/actions/ticket.actions';

@Component({
  selector: 'new-ticket',
  templateUrl: './new-ticket.component.html',
})
export class NewTicketComponent implements OnInit {
  newTicketForm = new FormGroup({
    departureCityName: new FormControl(''),
    departureDate: new FormControl(''),
    arrivalCityName: new FormControl(''),
    arrivalDate: new FormControl('')
  });

  constructor(private _store: Store<IAppState>) { }

  ngOnInit() { }

  onSubmit() {
    this._store.dispatch(new CreateTicket(this.newTicketForm.value))
  }
}