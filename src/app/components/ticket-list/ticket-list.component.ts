import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Store, select, createSelector } from '@ngrx/store';

import { IAppState } from 'src/app/store/state/app.state';
import { ITicketState } from 'src/app/store/state/ticket.state';
import { GetTickets } from 'src/app/store/actions/ticket.actions';

@Component({
  selector: 'ticket-list',
  templateUrl: './ticket-list.component.html',
  styleUrls: ['./ticket-list.component.css']
})
export class TicketListComponent implements OnInit {
  selectTicketList = createSelector(
    (state: IAppState) => state.tickets,
    (state: ITicketState) => state.tickets
  );

  tickets$ = this._store.pipe(select(this.selectTicketList))

  constructor(private _store: Store<IAppState>) { }

  ngOnInit() {
    this._store.dispatch(new GetTickets());
   }
}
