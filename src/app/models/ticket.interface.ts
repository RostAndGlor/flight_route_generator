export interface ITicket {
  departureCityName: string;
  departureTime: Date;
  arrivalCityName: string;
  arrivalTime: Date
}
